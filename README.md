cd ~/projects/safe_animals && \
git pull origin master && \
docker-compose down && \
make build-image TAG=1.0 && \
make push-image && \
docker-compose up -d