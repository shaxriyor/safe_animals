package postgres

import (
	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type researchRepo struct {
	db *sqlx.DB
}

func NewResearchRepo(db *sqlx.DB) repo.ResearchRepoI {
	return researchRepo{db: db}
}

func (r researchRepo) Create(req models.Research) (res models.ResponseWithID, err error) {
	tx, err := r.db.Begin()
	if err != nil {
		return
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		} else {
			_ = tx.Commit()
		}
	}()

	queryCreature := `INSERT INTO creatures(name, description, remaining_count, sub_type_id) VALUES ($1, $2, $3, $4) RETURNING id; `
	err = tx.QueryRow(queryCreature, req.Creature.Name, req.Creature.Name, req.Creature.RemainingCount, req.Creature.SubTypeID).Scan(&req.Creature.ID)
	if err != nil {
		return
	}

	query := `INSERT INTO researches(creature_id, user_id) VALUES($1,$2) RETURNING id; `
	err = tx.QueryRow(query, req.Creature.ID, req.UserID).Scan(&res.ID)
	if err != nil {
		return
	}

	return
}

func (r researchRepo) Update(req models.Research) (*models.ResponseWithID, error) {
	tx, err := r.db.Begin()
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		} else {
			_ = tx.Commit()
		}
	}()

	queryUpdateCreature := `UPDATE creatures SET name=$1, description=$2, remaining_count=$3, sub_type_id=$4 WHERE id=$5 `
	_, err = tx.Exec(queryUpdateCreature, req.Creature.Name, req.Creature.Description, req.Creature.RemainingCount, req.Creature.SubTypeID, req.ID)
	if err != nil {
		return nil, err
	}

	query := `UPDATE researches SET 
					creature_id = $1,
					user_id = $2
			 WHERE id = $3 `
	_, err = r.db.Exec(query, req.Creature.ID, req.UserID, req.ID)
	if err != nil {
		return nil, err
	}

	return &models.ResponseWithID{ID: req.ID}, nil
}

func (r researchRepo) Delete(id int64) (res models.ResponseWithID, err error) {
	query := `DELETE FROM researches WHERE id = $1 RETURNING id `
	err = r.db.Get(&res.ID, query, id)
	return
}

func (r researchRepo) Get(id int64) (res models.Research, err error) {
	res.ID = id

	query := `
		SELECT
			c.id, 
			c.name,
			COALESCE(c.description, ''),
			c.remaining_count,
			c.sub_type_id,
			r.user_id 
		FROM researches r 
		INNER JOIN creatures c ON c.id=r.creature_id
		WHERE r.id = $1 `
	err = r.db.QueryRow(query, id).Scan(
		&res.Creature.ID,
		&res.Creature.Name,
		&res.Creature.Description,
		&res.Creature.RemainingCount,
		&res.Creature.SubTypeID,
		&res.UserID,
	)
	return
}

func (r researchRepo) GetAll(filters models.GetAllResearchesRequest) (models.Researches, error) {
	var (
		count  int64
		column string
	)

	res := make([]models.Research, 0)

	offset := (filters.Page - 1) * filters.Limit

	params := map[string]interface{}{
		"user_id":     filters.UserID,
		"limit":       filters.Limit,
		"offset":      offset,
		"sub_type_id": filters.SubTypeID,
	}

	if filters.UserID != 0 {
		column += ` AND c.user_id =:user_id `
	}

	if filters.SubTypeID != 0 {
		column += ` AND c.sub_type_id =:sub_type_id `
	}

	query := `
		SELECT 
			r.id, 
			c.id,
			c.name,
			COALESCE(c.description, ''),
			c.remaining_count,
			c.sub_type_id,
			r.user_id 
		FROM researches r 
		INNER JOIN creatures c ON c.id=r.creature_id
		WHERE 1=1 ` + column + `
	 ORDER BY r.created_at desc LIMIT :limit OFFSET :offset `
	rows, err := r.db.NamedQuery(query, params)
	if err != nil {
		return models.Researches{Researches: res}, err
	}
	defer rows.Close()

	for rows.Next() {
		var r models.Research

		err = rows.Scan(
			&r.ID,
			&r.Creature.ID,
			&r.Creature.Name,
			&r.Creature.Description,
			&r.Creature.RemainingCount,
			&r.Creature.SubTypeID,
			&r.UserID,
		)
		if err != nil {
			return models.Researches{Researches: res}, err
		}

		res = append(res, r)
	}

	queryCount := `SELECT count(1) FROM researches r INNER JOIN creatures c ON c.id=r.creature_id WHERE 1=1 ` + column
	stmt, err := r.db.PrepareNamed(queryCount)
	if err != nil {
		return models.Researches{Researches: res}, err
	}

	err = stmt.QueryRow(params).Scan(&count)
	if err != nil {
		return models.Researches{Researches: res}, err
	}

	return models.Researches{Researches: res, Count: int(count)}, nil
}
