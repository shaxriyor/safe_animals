package postgres

import (
	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type creatureRepo struct {
	db *sqlx.DB
}

func NewCreatureRepo(db *sqlx.DB) repo.CreatureRepoI {
	return creatureRepo{db: db}
}

func (r creatureRepo) GetAll(filters models.GetAllCreaturesRequest) (models.Creatures, error) {
	var (
		column string
		res    models.Creatures
	)

	offset := (filters.Page - 1) * filters.Limit

	params := map[string]interface{}{
		"search": filters.Search,
		"limit":  filters.Limit,
		"offset": offset,
	}

	if filters.Search != "" {
		column += ` AND name ILIKE '%' || :search || '%' `
	}

	query := `
		SELECT 
			id,
			name,
			COALESCE(description, ''),
			sub_type_id,
			remaining_count
		FROM creatures WHERE status=1 ` + column + `
		ORDER BY created_at LIMIT :limit OFFSET :offset
	`

	rows, err := r.db.NamedQuery(query, params)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var c models.Creature

		err = rows.Scan(
			&c.ID,
			&c.Name,
			&c.Description,
			&c.SubTypeID,
			&c.RemainingCount,
		)
		if err != nil {
			return res, err
		}

		res.Creatures = append(res.Creatures, c)
	}

	queryCount := `SELECT count(1) FROM creatures WHERE status=1 ` + column
	stmt, err := r.db.PrepareNamed(queryCount)
	if err != nil {
		return res, err
	}

	err = stmt.QueryRow(params).Scan(&res.Count)

	return res, err
}
