package postgres

import (
	"database/sql"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type confirmationRepo struct {
	db *sqlx.DB
}

func NewConfirmationRepo(db *sqlx.DB) repo.ConfirmationRepoI {
	return confirmationRepo{db: db}
}

func (r confirmationRepo) Create(req models.ConfirmationResponse) (res models.ResponseWithID, err error) {
	tx, err := r.db.Begin()
	if err != nil {
		return res, err
	}

	defer func() {
		if err != nil {
			_ = tx.Rollback()
		} else {
			_ = tx.Commit()
		}
	}()

	query := `INSERT INTO confirmation_responses(research_id, is_confirmed, comment, coins_amount, confirmed_by) VALUES($1, $2, $3, $4, $5) RETURNING id; `
	err = tx.QueryRow(query, req.ResearchID, req.IsConfirmed, req.Comment, req.CoinsAmount, req.ConfirmedBy).Scan(&res.ID)
	if err != nil {
		return res, err
	}

	if !req.IsConfirmed {
		return res, nil
	}

	err = r.updateCreatureStatus(tx, int(req.ResearchID))
	if err != nil {
		return
	}

	err = r.updateUserRank(tx, int(req.ResearchID))
	if err != nil {
		return res, err
	}

	return res, nil
}

func (r confirmationRepo) Get(researchID int) (models.ConfirmationResponse, error) {
	var res models.ConfirmationResponse

	query := `SELECT id, is_confirmed, comment, coins_amount FROM confirmation_responses WHERE research_id=$1`
	err := r.db.Get(&res, query, researchID)
	return res, err
}

func (r confirmationRepo) GetAll(filters models.GetAllConfirmationsRequest) (models.Confirmations, error) {
	var (
		res    models.Confirmations
		column string
	)

	if filters.IsConfirmed == "true" {
		column += " AND is_confirmed=true "
	} else if filters.IsConfirmed == "false" {
		column += " AND is_confirmed=false "
	}

	offset := (filters.Page - 1) * filters.Limit

	query := `
		SELECT id, research_id, is_confirmed, comment, coins_amount, confirmed_by FROM confirmation_responses
		WHERE 1=1 ` + column + `
		ORDER BY created_at LIMIT $1 OFFSET $2	
	`
	rows, err := r.db.Query(query, filters.Limit, offset)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		var c models.ConfirmationResponse

		err = rows.Scan(
			&c.ID,
			&c.ResearchID,
			&c.IsConfirmed,
			&c.Comment,
			&c.CoinsAmount,
			&c.ConfirmedBy,
		)
		if err != nil {
			return res, err
		}

		res.Confirmations = append(res.Confirmations, c)
	}

	queryCount := `SELECT count(1) FROM confirmation_responses WHERE 1=1 ` + column
	err = r.db.QueryRow(queryCount).Scan(&res.Count)

	return res, err
}

func (r confirmationRepo) updateCreatureStatus(tx *sql.Tx, researchID int) error {
	res, err := tx.Exec("UPDATE creatures SET status=1 WHERE id = (SELECT creature_id FROM researches WHERE id=$1)", researchID)
	if err != nil {
		return err
	}

	if rows, _ := res.RowsAffected(); rows == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (r confirmationRepo) updateUserRank(tx *sql.Tx, researchID int) error {
	var (
		coins         int
		rankID        int
		userRankCount int
		newUserRankID int
	)

	// select user's total coins amount
	query := `
		SELECT sum(cr.coins_amount) 
		FROM confirmation_responses cr
		LEFT JOIN researches r ON cr.research_id=r.id
		WHERE r.user_id = (select user_id from researches where id=$1) and cr.is_confirmed=true `

	err := tx.QueryRow(query, researchID).Scan(&coins)
	if err != nil {
		return err
	}

	// select biggest rank for this user
	err = tx.QueryRow("SELECT id FROM rank_level WHERE min_coins >= $1 ORDER BY rank_level DESC LIMIT 1", coins).Scan(&rankID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}

	// check if user already has this rank
	err = tx.QueryRow("SELECT count(1) FROM user_rank WHERE user_id=(SELECT user_id FROM researches WHERE id=$1) and rank_level_id=$2", researchID, rankID).Scan(&userRankCount)
	if err != nil {
		return err
	}

	if userRankCount != 0 {
		return nil
	}

	// insert new user rank
	err = tx.QueryRow(`INSERT INTO user_rank(user_id, rank_id, is_active) VALUES ((SELECT user_id FROM researches WHERE id=$1), $2, true) RETURNING id`, researchID, rankID).Scan(&newUserRankID)
	if err != nil {
		return err
	}

	// change old ranks to inactive
	_, err = tx.Exec(`UPDATE user_rank SET is_active=false WHERE user_id=(SELECT user_id FROM researches WHERE id=$1) and rank_level_id!=$2`, researchID, newUserRankID)
	return err
}
