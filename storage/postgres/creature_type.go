package postgres

import (
	"database/sql"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type creatureTypeRepo struct {
	db *sqlx.DB
}

func NewCreatureTypepRepo(db *sqlx.DB) repo.CreatureTypeRepoI {
	return creatureTypeRepo{db: db}
}

func (b creatureTypeRepo) Create(req models.CreatureType) (res models.ResponseWithID, err error) {
	query := `INSERT INTO creature_types(name, alias) VALUES($1,$2) RETURNING id`
	err = b.db.Get(&res.ID, query, req.Name, req.Alias)
	return
}

func (d creatureTypeRepo) Update(req models.CreatureType) (*models.ResponseWithID, error) {
	query := `UPDATE creature_types SET 
					name = $1,
					alias = $2,
					updated_at = now()
			 WHERE id = $3`
	res, err := d.db.Exec(query, req.Name, req.Alias, req.ID)
	if err != nil {
		return nil, err
	}
	rows, _ := res.RowsAffected()
	if rows == 0 {
		return nil, sql.ErrNoRows
	}

	return &models.ResponseWithID{ID: req.ID}, nil
}

func (b creatureTypeRepo) Delete(id int64) (res models.ResponseWithID, err error) {
	query := `DELETE FROM creature_types WHERE id = $1 RETURNING id`
	err = b.db.Get(&res.ID, query, id)
	return
}

func (d creatureTypeRepo) Get(id int64) (res models.CreatureType, err error) {
	query := `SELECT id, alias, name FROM creature_types WHERE id = $1`
	err = d.db.Get(&res, query, id)
	return
}

func (d creatureTypeRepo) GetAll(limit, page int32) (models.CreatureTypes, error) {
	var count int64
	res := []models.CreatureType{}

	offset := (page - 1) * limit

	query := `SELECT id, alias, name FROM creature_types ORDER BY created_at desc LIMIT $1 OFFSET $2`
	err := d.db.Select(&res, query, limit, offset)
	if err != nil {
		return models.CreatureTypes{CreatureType: res}, err
	}

	queryCount := `SELECT count(1) FROM creature_types LIMIT $1 OFFSET $2`
	err = d.db.Get(&count, queryCount, limit, offset)
	if err != nil {
		return models.CreatureTypes{CreatureType: res}, err

	}
	return models.CreatureTypes{CreatureType: res, Count: count}, nil
}

func (d creatureTypeRepo) GetAllSubTypes(limit, page int32) (models.GetAllSubTypes, error) {
	var count int64
	res := []models.SubType{}

	offset := (page - 1) * limit

	query := `SELECT id, alias, name, creature_type_id FROM creature_sub_type LIMIT $1 OFFSET $2`
	err := d.db.Select(&res, query, limit, offset)
	if err != nil {
		return models.GetAllSubTypes{SubTypes: res}, err
	}

	queryCount := `SELECT count(1) FROM creature_sub_type LIMIT $1 OFFSET $2`
	err = d.db.Get(&count, queryCount, limit, offset)
	if err != nil {
		return models.GetAllSubTypes{SubTypes: res}, err

	}
	return models.GetAllSubTypes{SubTypes: res, Count: count}, nil
}
