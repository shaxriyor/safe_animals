package postgres

import (
	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type authRepo struct {
	db *sqlx.DB
}

func NewAuthRepo(db *sqlx.DB) repo.AuthRepoI {
	return authRepo{db: db}
}

func (a authRepo) Register(req models.Register) (res models.ResponseWithID, err error) {
	query := `INSERT INTO users(full_name, user_type_id, login, password) VALUES($1,(select id from user_type where alias='visitors'), $2,$3) RETURNING id`
	err = a.db.Get(&res.ID, query, req.FullName, req.Login, req.Password)
	return
}

func (a authRepo) Login(req models.Login) (res models.LoginResponse, err error) {
	query := `SELECT u.id user_id, ut.alias user_type
			  FROM users u
			  JOIN user_type ut ON ut.id = u.user_type_id
			  WHERE login = $1 and password = $2`
	err = a.db.Get(&res, query, req.Login, req.Password)
	return
}
