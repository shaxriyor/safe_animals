package postgres

import (
	"database/sql"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) repo.UserRepoI {
	return userRepo{db: db}
}

func (r userRepo) Create(req models.User) (res models.ResponseWithID, err error) {
	query := `INSERT INTO users(full_name, user_type_id, login, password, sub_type_id) VALUES($1,$2, $3,$4,$5) RETURNING id`
	err = r.db.Get(&res.ID, query, req.FullName, req.UserTypeID, req.Login, req.Password, req.SubTypeID)

	return
}

func (r userRepo) Update(req models.User) (*models.ResponseWithID, error) {
	query := `UPDATE users SET 
					full_name = $1,
					user_type_id = $2,
					login = $3, 
					password = $4, 
					sub_type_id = $5
			 WHERE id = $6 `
	res, err := r.db.Exec(query, req.FullName, req.UserTypeID, req.Login, req.Password, req.SubTypeID, req.ID)
	if err != nil {
		return nil, err
	}

	rows, _ := res.RowsAffected()
	if rows == 0 {
		return nil, sql.ErrNoRows
	}

	return &models.ResponseWithID{ID: req.ID}, nil
}

func (r userRepo) Delete(id int64) (res models.ResponseWithID, err error) {
	query := `DELETE FROM users WHERE id = $1 RETURNING id`
	err = r.db.Get(&res.ID, query, id)
	return
}

func (r userRepo) Get(id int64) (res models.GetUserResponse, err error) {
	query := `SELECT u.id id, full_name, user_type_id, ut.name user_type, login, password, st.id sub_type_id, st.name sub_type
			  FROM users u
			  LEFT JOIN creature_sub_type st ON u.sub_type_id = st.id
			  LEFT JOIN user_type ut ON u.user_type_id = ut.id
			  WHERE u.id = $1`
	err = r.db.Get(&res, query, id)
	return
}

func (r userRepo) GetAll(limit, page int32) (models.Users, error) {
	var count int64
	res := make([]models.GetUserResponse, 0)

	offset := (page - 1) * limit

	query := `SELECT u.id id, full_name, user_type_id, COALESCE(ut.alias, '') user_type, login, password, st.id sub_type_id, st.name sub_type
			  FROM users u
			  LEFT JOIN creature_sub_type st ON u.sub_type_id = st.id
			  LEFT JOIN user_type ut ON u.user_type_id = ut.id ORDER BY u.created_at desc LIMIT $1 OFFSET $2 `
	err := r.db.Select(&res, query, limit, offset)
	if err != nil {
		return models.Users{Users: res}, err
	}

	queryCount := `SELECT count(1) 
				   FROM users u
				   LEFT JOIN creature_sub_type st ON u.id = st.id
				   LEFT JOIN user_type ut ON u.id = ut.id
				   LIMIT $1 OFFSET $2`
	err = r.db.Get(&count, queryCount, limit, offset)
	if err != nil {
		return models.Users{Users: res}, err

	}
	return models.Users{Users: res, Count: count}, nil
}

func (r userRepo) GetAllUserType(limit, page int32) (models.GetAllUserType, error) {
	var count int64
	res := make([]models.UserType, 0)

	offset := (page - 1) * limit

	query := `SELECT id, name, alias FROM user_type LIMIT $1 OFFSET $2 `
	err := r.db.Select(&res, query, limit, offset)
	if err != nil {
		return models.GetAllUserType{UserTypes: res}, err
	}

	queryCount := `SELECT count(1) FROM user_type LIMIT $1 OFFSET $2`
	err = r.db.Get(&count, queryCount, limit, offset)
	if err != nil {
		return models.GetAllUserType{UserTypes: res}, err

	}
	return models.GetAllUserType{UserTypes: res, Count: count}, nil
}
