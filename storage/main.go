package storage

import (
	// "github.com/Shakhrik/inha/bus_tracking/api/storage/postgres"
	// "github.com/Shakhrik/inha/bus_tracking/api/storage/repo"
	"github.com/Shakhrik/inha/bus_tracking/storage/postgres"
	"github.com/Shakhrik/inha/bus_tracking/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	CreatureTypepRepo() repo.CreatureTypeRepoI
	User() repo.UserRepoI
	Research() repo.ResearchRepoI
	Confirmation() repo.ConfirmationRepoI
	Auth() repo.AuthRepoI
	Creature() repo.CreatureRepoI
}

type storagePG struct {
	db               *sqlx.DB
	creatureTypeRepo repo.CreatureTypeRepoI
	userRepo         repo.UserRepoI
	researchRepo     repo.ResearchRepoI
	confirmationRepo repo.ConfirmationRepoI
	authRepo         repo.AuthRepoI
	creatureRepo     repo.CreatureRepoI
}

func NewStoragePG(db *sqlx.DB) StorageI {
	return storagePG{
		db:               db,
		creatureTypeRepo: postgres.NewCreatureTypepRepo(db),
		userRepo:         postgres.NewUserRepo(db),
		researchRepo:     postgres.NewResearchRepo(db),
		confirmationRepo: postgres.NewConfirmationRepo(db),
		authRepo:         postgres.NewAuthRepo(db),
		creatureRepo:     postgres.NewCreatureRepo(db),
	}
}

func (s storagePG) CreatureTypepRepo() repo.CreatureTypeRepoI {
	return s.creatureTypeRepo
}

func (s storagePG) Research() repo.ResearchRepoI {
	return s.researchRepo
}

func (s storagePG) Confirmation() repo.ConfirmationRepoI {
	return s.confirmationRepo
}

func (s storagePG) User() repo.UserRepoI {
	return s.userRepo
}

func (s storagePG) Auth() repo.AuthRepoI {
	return s.authRepo
}

func (s storagePG) Creature() repo.CreatureRepoI {
	return s.creatureRepo
}
