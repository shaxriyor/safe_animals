package repo

import "github.com/Shakhrik/inha/bus_tracking/api/models"

type ResearchRepoI interface {
	Create(req models.Research) (res models.ResponseWithID, err error)
	Update(req models.Research) (*models.ResponseWithID, error)
	Delete(id int64) (res models.ResponseWithID, err error)
	Get(id int64) (res models.Research, err error)
	GetAll(models.GetAllResearchesRequest) (models.Researches, error)
}
