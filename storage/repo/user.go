package repo

import "github.com/Shakhrik/inha/bus_tracking/api/models"

type UserRepoI interface {
	Create(req models.User) (res models.ResponseWithID, err error)
	Update(req models.User) (*models.ResponseWithID, error)
	Delete(id int64) (res models.ResponseWithID, err error)
	Get(id int64) (res models.GetUserResponse, err error)
	GetAll(limit, page int32) (models.Users, error)
	GetAllUserType(limit, page int32) (models.GetAllUserType, error)
}
