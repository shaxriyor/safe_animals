package repo

import "github.com/Shakhrik/inha/bus_tracking/api/models"

type CreatureTypeRepoI interface {
	Create(req models.CreatureType) (res models.ResponseWithID, err error)
	Update(req models.CreatureType) (*models.ResponseWithID, error)
	Delete(id int64) (res models.ResponseWithID, err error)
	Get(id int64) (res models.CreatureType, err error)
	GetAll(limit, page int32) (models.CreatureTypes, error)

	//Creature Sub type
	GetAllSubTypes(limit, page int32) (models.GetAllSubTypes, error)
}
