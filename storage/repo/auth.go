package repo

import "github.com/Shakhrik/inha/bus_tracking/api/models"

type AuthRepoI interface {
	Register(req models.Register) (res models.ResponseWithID, err error)
	Login(req models.Login) (res models.LoginResponse, err error)
}
