package repo

import "github.com/Shakhrik/inha/bus_tracking/api/models"

type ConfirmationRepoI interface {
	Create(req models.ConfirmationResponse) (res models.ResponseWithID, err error)
	Get(researchID int) (models.ConfirmationResponse, error)
	GetAll(filters models.GetAllConfirmationsRequest) (models.Confirmations, error)
}
