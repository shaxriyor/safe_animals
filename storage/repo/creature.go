package repo

import "github.com/Shakhrik/inha/bus_tracking/api/models"

type CreatureRepoI interface {
	GetAll(filters models.GetAllCreaturesRequest) (models.Creatures, error)
}
