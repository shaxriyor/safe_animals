FROM golang:1.18 as builder


#
RUN mkdir -p $GOPATH/src/inha/safe_animals
WORKDIR $GOPATH/src/inha/safe_animals

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/safe_animals /

ENTRYPOINT ["/safe_animals"]

