package v1

import (
	"net/http"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/gin-gonic/gin"
)

//@Router /v1/login [post]
//@Summary Login
//@Description API for Login
//@Tags auth
//@Accept json
//@Produce json
//@Param employee body models.Login true "login"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) Login(c *gin.Context) {
	var entity models.Login

	err := c.ShouldBind(&entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	res, err := h.storage.Auth().Login(entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 201, "visitor logged in successfully", res)
}

//@Router /v1/register [post]
//@Summary Register
//@Description API for register
//@Tags auth
//@Accept json
//@Produce json
//@Param employee body models.Register true "register"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) Register(c *gin.Context) {
	var entity models.Register

	err := c.ShouldBind(&entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	res, err := h.storage.Auth().Register(entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 201, "visitor registered successfully", res)
}
