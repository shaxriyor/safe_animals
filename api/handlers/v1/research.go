package v1

import (
	"net/http"
	"strconv"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/gin-gonic/gin"
)

//@Router /v1/research [post]
//@Summary Create research
//@Description API for creating research
//@Tags research
//@Accept json
//@Produce json
//@Param entity body models.Research true "entity"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreateResearch(c *gin.Context) {
	var entity models.Research

	err := c.ShouldBind(&entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	res, err := h.storage.Research().Create(entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 201, "created successfully", res)
}

//@Router /v1/research/{id} [put]
//@Summary Update research
//@Description API for updating research
//@Tags research
//@Accept json
//@Produce json
//@Param id path string true "id"
//@Param entity body models.Research true "entity"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) UpdateResearch(c *gin.Context) {
	var entity models.Research

	err := c.ShouldBind(&entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	desID := c.Param("id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	entity.ID = int64(value)
	res, err := h.storage.Research().Update(entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "updated successfully", res)
}

//@Router /v1/research/{id} [delete]
//@Summary Delete research
//@Description API for deleting research
//@Tags research
//@Accept json
//@Produce json
//@Param id path string true "id"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) DeleteResearch(c *gin.Context) {
	desID := c.Param("id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.Research().Delete(int64(value))
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "deleted successfully", res)
}

//@Router /v1/research/{id} [get]
//@Summary Get research
//@Description API for getting research
//@Tags research
//@Accept json
//@Produce json
//@Param id path string true "id"
//@Success 200 {object} models.Research
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) GetResearch(c *gin.Context) {
	desID := c.Param("id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.Research().Get(int64(value))
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "success", res)
}

//@Router /v1/research [get]
//@Summary GetAll researchs
//@Description API for getting all researchs
//@Tags research
//@Accept json
//@Produce json
//@Param entity query models.GetAllResearchesRequest true "entity"
//@Success 200 {object} models.Researches
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) GetAllResearches(c *gin.Context) {
	limit, err := ParseQueryParam(c, "limit", "100")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	page, err := ParseQueryParam(c, "page", "1")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	userID, err := ParseQueryParam(c, "user_id", "0")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	subTypeID, err := ParseQueryParam(c, "sub_type_id", "0")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.Research().GetAll(models.GetAllResearchesRequest{
		Page:      page,
		Limit:     limit,
		UserID:    userID,
		SubTypeID: subTypeID,
	})
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "success", res)
}

//@Router /v1/research-confirmation [post]
//@Summary Confirm research
//@Description API for confirming research
//@Tags research
//@Accept json
//@Produce json
//@Param entity body models.ConfirmationResponse true "entity"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) ConfirmResearch(c *gin.Context) {
	var entity models.ConfirmationResponse

	err := c.ShouldBind(&entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	res, err := h.storage.Confirmation().Create(entity)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "updated successfully", res)
}

//@Router /v1/research-confirmation/{research_id} [get]
//@Summary Get research confirmation
//@Description API for getting research confirmation
//@Tags research
//@Accept json
//@Produce json
//@Param research_id path string true "research_id"
//@Success 200 {object} models.ConfirmationResponse
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) GetConfirmation(c *gin.Context) {
	desID := c.Param("research_id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.Confirmation().Get(value)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "success", res)
}

//@Router /v1/research-confirmation [get]
//@Summary Get All research confirmations
//@Description API for getting all research confirmation
//@Tags research
//@Accept json
//@Produce json
//@Param filters query models.GetAllConfirmationsRequest false "filters"
//@Success 200 {object} models.Confirmations
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) GetAllConfirmations(c *gin.Context) {
	limit, err := ParseQueryParam(c, "limit", "100")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	page, err := ParseQueryParam(c, "page", "1")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.Confirmation().GetAll(models.GetAllConfirmationsRequest{
		Page:        page,
		Limit:       limit,
		IsConfirmed: c.Query("is_confirmed"),
	})
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "success", res)
}
