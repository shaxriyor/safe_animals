package v1

import (
	"net/http"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/gin-gonic/gin"
)

//@Router /v1/creatures [get]
//@Summary GetAll creatures
//@Description API for getting all creatures
//@Tags creatures
//@Accept json
//@Produce json
//@Param filters query models.GetAllCreaturesRequest false "filters"
//@Success 200 {object} models.Creatures
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) GetAllCreatures(c *gin.Context) {
	limit, err := ParseQueryParam(c, "limit", "100")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	page, err := ParseQueryParam(c, "page", "1")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.Creature().GetAll(models.GetAllCreaturesRequest{
		Page:   page,
		Limit:  limit,
		Search: c.Query("search"),
	})
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "success", res)
}
