package v1

import (
	"net/http"
	"strconv"

	"github.com/Shakhrik/inha/bus_tracking/api/models"
	"github.com/gin-gonic/gin"
)

//@Router /v1/creature-type [post]
//@Summary Create creature type
//@Description API for create creature type
//@Tags creature-type
//@Accept json
//@Produce json
//@Param employee body models.CreatureType true "creature-type"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreatureTypeCreate(c *gin.Context) {
	var creatureTypeCreate models.CreatureType

	err := c.ShouldBind(&creatureTypeCreate)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	res, err := h.storage.CreatureTypepRepo().Create(creatureTypeCreate)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 201, "creature-type created successfully", res)
}

//@Router /v1/creature-type/{id} [put]
//@Summary Update creature-type
//@Description API for updating creature-type
//@Tags creature-type
//@Accept json
//@Produce json
//@Param id path string true "id"
//@Param employee body models.CreatureType true "creature-type"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreatureTypeUpdate(c *gin.Context) {
	var creatureTypeUpdate models.CreatureType

	err := c.ShouldBind(&creatureTypeUpdate)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusBadRequest, "bad request", err)
		return
	}

	desID := c.Param("id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	creatureTypeUpdate.ID = int64(value)
	res, err := h.storage.CreatureTypepRepo().Update(creatureTypeUpdate)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "destination updated successfully", res)
}

//@Router /v1/creature-type/{id} [delete]
//@Summary Delete creature-type
//@Description API for deleting creature-type
//@Tags creature-type
//@Accept json
//@Produce json
//@Param id path string true "id"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreatureTypeDelete(c *gin.Context) {
	desID := c.Param("id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.CreatureTypepRepo().Delete(int64(value))
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "creature-type deleted successfully", res)
}

//@Router /v1/creature-type/{id} [get]
//@Summary Get creature-type
//@Description API for getting creature-type
//@Tags creature-type
//@Accept json
//@Produce json
//@Param id path string true "id"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreatureTypeGet(c *gin.Context) {
	desID := c.Param("id")
	value, err := strconv.Atoi(desID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.CreatureTypepRepo().Get(int64(value))
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "get successfully", res)
}

//@Router /v1/creature-type [get]
//@Summary GetAll creature-types
//@Description API for getting all creature-types
//@Tags creature-type
//@Accept json
//@Produce json
//@Param limit query int false "limit"
//@Param page query int false "page"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreatureTypeGetAll(c *gin.Context) {
	limit, err := ParseQueryParam(c, "limit", "100")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	page, err := ParseQueryParam(c, "page", "1")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.CreatureTypepRepo().GetAll(limit, page)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "get all successfully", res)
}

//@Router /v1/creature-sub-type [get]
//@Summary GetAll creature-sub-types
//@Description API for getting all creature-sub-types
//@Tags creature-sub-type
//@Accept json
//@Produce json
//@Param limit query int false "limit"
//@Param page query int false "page"
//@Success 200 {object} models.SuccessModel
//@Failure 400 {object} models.ResponseError
//@Failure 500 {object} models.ResponseError
func (h handlerV1) CreatureSubTypeGetAll(c *gin.Context) {
	limit, err := ParseQueryParam(c, "limit", "100")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	page, err := ParseQueryParam(c, "page", "1")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	res, err := h.storage.CreatureTypepRepo().GetAllSubTypes(limit, page)
	if err != nil {
		h.HandleErrorResponse(c, http.StatusInternalServerError, "database error", err)
		return
	}

	h.HandleSuccessResponse(c, 200, "get all successfully", res)
}
