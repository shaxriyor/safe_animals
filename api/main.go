package api

import (
	"github.com/Shakhrik/inha/bus_tracking/config"
	"github.com/Shakhrik/inha/bus_tracking/pkg/logger"
	"github.com/Shakhrik/inha/bus_tracking/storage"

	v1 "github.com/Shakhrik/inha/bus_tracking/api/handlers/v1"

	_ "github.com/Shakhrik/inha/bus_tracking/api/docs"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type RouterOptions struct {
	Log     logger.Logger
	Cfg     *config.Config
	Storage storage.StorageI
}

func New(opt *RouterOptions) *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	cfg := cors.DefaultConfig()

	cfg.AllowHeaders = append(cfg.AllowHeaders, "*")
	cfg.AllowAllOrigins = true
	cfg.AllowCredentials = true

	router.Use(cors.New(cfg))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Log:     opt.Log,
		Cfg:     opt.Cfg,
		Storage: opt.Storage,
	})

	apiV1 := router.Group("/v1")

	{
		apiV1.POST("/creature-type", handlerV1.CreatureTypeCreate)
		apiV1.PUT("/creature-type/:id", handlerV1.CreatureTypeUpdate)
		apiV1.DELETE("/creature-type/:id", handlerV1.CreatureTypeDelete)
		apiV1.GET("/creature-type/:id", handlerV1.CreatureTypeGet)
		apiV1.GET("/creature-type", handlerV1.CreatureTypeGetAll)

		apiV1.POST("/research", handlerV1.CreateResearch)
		apiV1.PUT("/research/:id", handlerV1.UpdateResearch)
		apiV1.DELETE("/research/:id", handlerV1.DeleteResearch)
		apiV1.GET("/research/:id", handlerV1.GetResearch)
		apiV1.GET("/research", handlerV1.GetAllResearches)

		apiV1.POST("/research-confirmation", handlerV1.ConfirmResearch)
		apiV1.GET("/research-confirmation/:research_id", handlerV1.GetConfirmation)
		apiV1.GET("/research-confirmation", handlerV1.GetAllConfirmations)

		apiV1.POST("/user", handlerV1.CreateUser)
		apiV1.PUT("/user/:id", handlerV1.UpdateUser)
		apiV1.DELETE("/user/:id", handlerV1.DeleteUser)
		apiV1.GET("/user/:id", handlerV1.GetUser)
		apiV1.GET("/user", handlerV1.GetAllUsers)

		apiV1.GET("/user-types", handlerV1.GetAllUserTypes)
		apiV1.GET("/creature-sub-type", handlerV1.CreatureSubTypeGetAll)

		apiV1.POST("/register", handlerV1.Register)
		apiV1.POST("/login", handlerV1.Login)

		apiV1.GET("/creatures", handlerV1.GetAllCreatures)

	}
	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
