package models

type ConfirmationResponse struct {
	ID          int64  `json:"id" db:"id" swaggerignore:"true"`
	ResearchID  int64  `json:"research_id" db:"research_id"`
	IsConfirmed bool   `json:"is_confirmed" db:"is_confirmed"`
	Comment     string `json:"comment" db:"comment"`
	CoinsAmount int    `json:"coins_amount" db:"coins_amount"`
	ConfirmedBy int    `json:"confirmed_by" db:"confirmed_by"`
}

type GetAllConfirmationsRequest struct {
	Page        int32  `json:"page"`
	Limit       int32  `json:"limit"`
	IsConfirmed string `json:"is_confirmed" example:"true,false"`
}

type Confirmations struct {
	Confirmations []ConfirmationResponse `json:"confirmations"`
	Count         int                    `json:"count"`
}
