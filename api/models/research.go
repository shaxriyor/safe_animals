package models

type Research struct {
	ID       int64    `json:"id" db:"id" swaggerignore:"true"`
	Creature Creature `json:"creature"`
	UserID   int      `json:"user_id" db:"user_id"`
}

type Researches struct {
	Researches []Research `json:"researches"`
	Count      int        `json:"count"`
}

type GetAllResearchesRequest struct {
	Page      int32 `json:"page"`
	Limit     int32 `json:"limit"`
	UserID    int32 `json:"user_id"`
	SubTypeID int32 `json:"sub_type_id"`
}
