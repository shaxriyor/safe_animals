package models

type Login struct {
	Login    string `json:"login" db:"login"`
	Password string `json:"password" db:"password"`
}

type Register struct {
	FullName string `json:"full_name" db:"full_name"`
	Login    string `json:"login" db:"login"`
	Password string `json:"password" db:"password"`
}

type LoginResponse struct {
	UserID   int    `json:"user_id" db:"user_id"`
	UserType string `json:"user_type" db:"user_type"`
}
