package models

type User struct {
	ID         int64  `json:"id" db:"id" swaggerignore:"true"`
	FullName   string `json:"full_name" db:"full_name"`
	UserTypeID int    `json:"user_type_id" db:"user_type_id"`
	Login      string `json:"login" db:"login"`
	Password   string `json:"password" db:"password"`
	SubTypeID  *int   `json:"sub_type_id" db:"sub_type_id"`
}

type GetUserResponse struct {
	ID         int64   `json:"id" db:"id" swaggerignore:"true"`
	FullName   string  `json:"full_name" db:"full_name"`
	UserTypeID int     `json:"user_type_id" db:"user_type_id"`
	UserType   string  `json:"user_type" db:"user_type"`
	Login      string  `json:"login" db:"login"`
	Password   string  `json:"password" db:"password"`
	SubTypeID  *int    `json:"sub_type_id" db:"sub_type_id"`
	Subtype    *string `json:"sub_type" db:"sub_type"`
}
type Users struct {
	Users []GetUserResponse `json:"users"`
	Count int64             `json:"count" db:"count"`
}

type GetAllUserType struct {
	UserTypes []UserType `json:"user_types"`
	Count     int64      `json:"count" db:"count"`
}

type UserType struct {
	ID    int64  `json:"id" db:"id" swaggerignore:"true"`
	Name  string `json:"name" db:"name"`
	Alias string `json:"alias" db:"alias"`
}
