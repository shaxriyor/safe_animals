package models

type Creature struct {
	ID             int64  `json:"id" db:"id" swaggerignore:"true"`
	Name           string `json:"name" db:"name"`
	RemainingCount int    `json:"remaining_count" db:"remaining_count"`
	Description    string `json:"description" db:"description"`
	SubTypeID      int    `json:"sub_type_id"`
}

type GetAllCreaturesRequest struct {
	Page   int32  `json:"page"`
	Limit  int32  `json:"limit"`
	Search string `json:"search"`
}

type Creatures struct {
	Creatures []Creature `json:"creatures"`
	Count     int        `json:"count"`
}
