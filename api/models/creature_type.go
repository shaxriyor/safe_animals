package models

type CreatureType struct {
	ID    int64  `json:"id" db:"id" swaggerignore:"true"`
	Name  string `json:"name" db:"name"`
	Alias string `json:"alias" db:"alias"`
}

type CreatureTypes struct {
	CreatureType []CreatureType `json:"creature_types"`
	Count        int64          `json:"count" db:"count"`
}

type SubType struct {
	ID             int64  `json:"id" db:"id" swaggerignore:"true"`
	Name           string `json:"name" db:"name"`
	Alias          string `json:"alias" db:"alias"`
	CreatureTypeID int64  `json:"creature_type_id" db:"creature_type_id"`
}

type GetAllSubTypes struct {
	SubTypes []SubType `json:"creature_sub_types"`
	Count    int64     `json:"count" db:"count"`
}
