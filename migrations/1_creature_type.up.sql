CREATE TABLE IF NOT EXISTS creature_types(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    alias VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS creature_sub_type(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    alias VARCHAR(255),
    creature_type_id INT REFERENCES creature_types(id) ON DELETE CASCADE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS creatures (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    sub_type_id INT REFERENCES creature_sub_type(id) ON DELETE CASCADE,
    remaining_count INT DEFAULT 0,
    description varchar,
    status smallint default 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
