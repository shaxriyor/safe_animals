CREATE TABLE IF NOT EXISTS user_type (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    alias VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS users(
    id SERIAL PRIMARY KEY,
    full_name VARCHAR(255),
    user_type_id int REFERENCES user_type(id),
    sub_type_id int REFERENCES creature_sub_type(id) DEFAULT null,
    login VARCHAR(50),
    password VARCHAR(50),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS researches (
    id SERIAL PRIMARY KEY,
    creature_id int REFERENCES creatures(id),
    user_id int REFERENCES users(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS confirmation_responses (
    id SERIAL PRIMARY KEY,
    research_id int REFERENCES researches(id) NOT NULL,
    is_confirmed BOOLEAN DEFAULT true,
    comment varchar(255),
    coins_amount int default 0,
    confirmed_by int REFERENCES users(id), 
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS rank_level (
    id SERIAL PRIMARY KEY,
    name VARCHAR,
    sequence_number INT DEFAULT 1,
    min_coins INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS user_rank (
    user_id INT REFERENCES users(id),
    rank_level_id INT REFERENCES rank_level(id),
    is_active BOOLEAN DEFAULT true,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

