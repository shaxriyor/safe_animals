INSERT INTO user_type(name, alias) VALUES
('Superadmin', 'superadmin'),
('Research Confirmers', 'research_confirmers'),
('Researchers', 'researchers'),
('Visitors', 'visitors');

INSERT INTO creature_types(name, alias) VALUES
('Animals', 'animals'),
('Birds', 'Birds'),
('Plants', 'plansts');

INSERT INTO creature_sub_type (name,creature_type_id, alias ) VALUES
('Land Animals', (select id from creature_types where alias = 'animals'), 'land_animals'),
('Water Animals', (select id from creature_types where alias = 'animals'), 'water_animals');
--this should be continued

INSERT INTO users(full_name, user_type_id, sub_type_id, login, password) VALUES
('Superadmin', (select id from user_type where alias='superadmin'), null, 'admin', 'admin1234'),
('Shakhriyor Ismatov', (select id from user_type where alias='researchers'), null, 'shakh', 'helloworld'),
('Abror Khamkhrokhujaev', (select id from user_type where alias='research_confirmers'), (select id from creature_sub_type where alias='land_animals'),'abror', 'abror2001');



INSERT INTO regions (name) VALUES
('Afghanistan'),
('Albania'),
('Algeria'),
('Andorra'),
('Angola'),
('Antigua and Barbuda'),
('Argentina'),
('Armenia'),
('Australia'),
('Austria'),
('Austrian Empire'),
('Azerbaijan'),
('Baden'),
('Bahamas The'),
('Bahrain'),
('Bangladesh'),
('Barbados'),
('Bavaria'),
('Belarus'),
('Belgium'),
('Belize'),
('Benin (Dahomey)'),
('Bolivia'),
('Bosnia and Herzegovina'),
('Botswana'),
('Brazil'),
('Brunei'),
('Brunswick and Lüneburg'),
('Bulgaria'),
('Burkina Faso (Upper Volta)'),
('Burma'),
('Burundi'),
('Cabo Verde'),
('Cambodia'),
('Cameroon'),
('Canada'),
('Cayman Islands The'),
('Central African Republic'),
('Central American Federation'),
('Chad'),
('Chile'),
('China'),
('Colombia'),
('Comoros'),
('Congo Free State The'),
('Costa Rica'),
('Cote d’Ivoire (Ivory Coast)'),
('Croatia'),
('Cuba'),
('Cyprus'),
('Czechia'),
('Czechoslovakia');